﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Fiddler.ClientCertificates
{
    public class MyCertificatesPlugin : IAutoTamper, IHandleExecAction
    {
        public const string MappingsKey = "mycertificates.plugin.mappings";
        public const string PluginDisabledKey = "mycertificates.plugin.disabled";
        public const string ExecActionMappingCommand = "mapping";
        public const string ExecActionEnableCommand = "enable";
        public const string ExecActionDisableCommand = "disable";
        public const string ExecActionHelpCommand = "help";
        public const string ExecActionNamespace = "mod";
        public const string HelpLink = "http://bit.ly/fldrccv100";

        protected IFiddlerPreferences Preferences { get; set; }

        public bool Disabled { get; private set; }

        private List<Mapping> mappings;

        public MyCertificatesPlugin()
        {
            this.mappings = new List<Mapping>();
            this.ResolveDependencies();
        }

        protected virtual void ResolveDependencies()
        {
            this.Preferences = FiddlerApplication.Prefs;
        }

        public void AutoTamperRequestAfter(Session oSession)
        {
            // Nothing to implement here yet...
        }

        public void AutoTamperRequestBefore(Session oSession)
        {
            if (this.Disabled)
            {
                return;
            }

            Mapping foundMapping = this.MatchMappingByHostname(oSession.hostname);
            if (foundMapping == null)
            {
                oSession.NoDecrypt().Set(bool.TrueString);
                return;
            }

            oSession.ClientCertificate().Set(foundMapping.CertificatePath);
        }

        public void AutoTamperResponseAfter(Session oSession)
        {
        }

        public void AutoTamperResponseBefore(Session oSession)
        {
        }

        public void OnBeforeReturningError(Session oSession)
        {
        }

        public void OnBeforeUnload()
        {
        }

        public void OnLoad()
        {
            this.ReadPreferences();
        }

        public Mapping MatchMappingByHostname(string hostname)
        {
            return this.mappings.SingleOrDefault(m => string.Equals(hostname, m.Hostname));
        }

        private void ReadPreferences()
        {
            this.mappings.Clear();

            string jsonString = this.Preferences.GetStringPref(MappingsKey, "[]");
            this.mappings.AddRange(JsonConvert.DeserializeObject<IEnumerable<Mapping>>(jsonString) ?? Enumerable.Empty<Mapping>());

            this.Disabled = this.Preferences.GetBoolPref(PluginDisabledKey, false);
        }

        public bool OnExecAction(string sCommand)
        {
            bool handled = this.IsSupportedCommand(sCommand);
            bool savePreferences = true;

            if (this.IsNewMappingCommand(sCommand))
            {
                string hostname = this.GetNewMappingCommandParam(sCommand, NewMappingParameter.Hostname);
                string thumbprint = this.GetNewMappingCommandParam(sCommand, NewMappingParameter.Thumbprint);
                string filePath = this.ExportPublicPartOfCertificate(thumbprint);
                this.mappings.Add(new Mapping() { Hostname = hostname, CertificatePath = filePath });
            }
            else if (this.IsHelpCommand(sCommand))
            {
                // No need to save preferences
                savePreferences = false;
                this.LaunchHyperlink(HelpLink);
            }
            else if (this.IsEnableCommand(sCommand))
            {
                this.Disabled = false;
            }
            else if (this.IsDisableCommand(sCommand))
            {
                this.Disabled = true;
            }

            if (handled && savePreferences)
            {
                this.SavePreferences();
            }

            return handled;
        }

        protected virtual void LaunchHyperlink(string hyperlink)
        {
            Utilities.LaunchHyperlink(hyperlink);
        }

        private bool IsHelpCommand(string sCommand)
        {
            return sCommand.OICStartsWith(ExecActionNamespace + " " + ExecActionHelpCommand);
        }

        private void SavePreferences()
        {
            string jsonString = JsonConvert.SerializeObject(this.mappings);
            this.Preferences.SetStringPref(MappingsKey, jsonString);
            this.Preferences.SetBoolPref(PluginDisabledKey, this.Disabled);
        }

        private bool IsDisableCommand(string sCommand)
        {
            return sCommand.OICStartsWith(ExecActionNamespace + " " + ExecActionDisableCommand);
        }

        private bool IsSupportedCommand(string sCommand)
        {
            return this.IsNewMappingCommand(sCommand) ||
                this.IsEnableCommand(sCommand) ||
                this.IsDisableCommand(sCommand) ||
                this.IsHelpCommand(sCommand);
        }

        private bool IsNewMappingCommand(string sCommand)
        {
            return sCommand.OICStartsWith(ExecActionNamespace + " " + ExecActionMappingCommand);
        }

        private bool IsEnableCommand(string sCommand)
        {
            return sCommand.OICStartsWith(ExecActionNamespace + " " + ExecActionEnableCommand);
        }

        private string GetNewMappingCommandParam(string sCommand, NewMappingParameter param)
        {
            string[] array = Utilities.Parameterize(sCommand);
            return array[(int)param];
        }

        private string ExportPublicPartOfCertificate(string thumbprint)
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            try
            {
                X509Certificate2 foundCertificate = null;

                store.Open(OpenFlags.ReadOnly);

                for (int i = 0; i < store.Certificates.Count; i++)
                {
                    if (store.Certificates[i].Thumbprint.OICEquals(thumbprint))
                    {
                        foundCertificate = store.Certificates[i];
                        break;
                    }
                }

                if (foundCertificate == null)
                {
                    throw new InvalidOperationException("Could not find certificate with the given thumbprint");
                }

                string filePath = this.ResolveCertificateFilePath(thumbprint);
                if (!this.PublicPartOfCertificateFileExists(filePath))
                {
                    this.SavePublicPartOfCertificate(foundCertificate, filePath);
                }

                return filePath;
            }
            finally
            {
                store.Close();
            }
        }

        private string ResolveCertificateFilePath(string thumbprint)
        {
            return @"c:\" + thumbprint + ".cer";
        }

        protected virtual bool PublicPartOfCertificateFileExists(string filePath)
        {
            return File.Exists(filePath);
        }

        protected virtual void SavePublicPartOfCertificate(X509Certificate2 foundCertificate, string filePath)
        {
            byte[] bytes = foundCertificate.Export(X509ContentType.Cert);
            File.WriteAllBytes(filePath, bytes);
        }
    }

    public enum NewMappingParameter
    {
        Hostname = 2,
        Thumbprint = 3
    }
}
