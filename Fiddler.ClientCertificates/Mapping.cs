﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Fiddler.ClientCertificates
{
    public class Mapping
    {
        [JsonProperty(PropertyName="h")]
        public string Hostname { get; set; }

        [JsonProperty(PropertyName = "p")]
        public string CertificatePath { get; set; }
    }
}
