﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiddler.ClientCertificates
{
    public static class SessionExtensions
    {
        public static SessionProperty ClientCertificate(this Session oSession)
        {
            return new SessionProperty(oSession, "https-Client-Certificate");
        }

        public static SessionProperty NoDecrypt(this Session oSession)
        {
            return new SessionProperty(oSession, "x-no-decrypt");
        }
    }

    public class SessionProperty
    {
        private readonly Session oSession;
        private readonly string flag;

        public SessionProperty(Session oSession, string flag)
        {
            this.oSession = oSession;
            this.flag = flag;
        }

        public string Get()
        {
            return this.oSession[this.flag];
        }

        public void Set(string value)
        {
            this.oSession[this.flag] = value;
        }
    }
}
