# README #

This project has been started from a single Fiddler plugin that I actually needed for one of my work projects, since Fiddler is the second tool I use currently the most :-).

The original idea of the plugin has come to me after dealing too much with swapping ClientCertificate.cer for different projects configurations and dozens of services my current project talks to. It has proven itself to be a tedious task to handle properly and very easy way to mess up your configuration/working state of the app. 

After reading a few chapters in the Fiddler book, I have came across Session["https-Client-Certificate"] flag that seems to be exactly what you need to deal with switching between client auth certificates on-the-per-request basis.

The way plugin works is very easy, first you configure it using **Add Host Certificate Auth Mapping Command**, which essentially exports public part of the certificate with the given thumbprint and creates a new mapping using the hostname specified. These mappings are kept in memory as a dictionary using the following schema: hostname (eq. mywebservice.com) => authcertpath.cer (eq. c:\XXXXXX.cer). Since the plugin interrogates with an every request it has a chance to validate whether the hostname matches any of the mappings in memory and set "https-Client-Certificate" flag for the given session with the corresponding client auth certificate if match is found. Very simple and handy.

### What is this repository for? ###

Any useful Fiddler plugin you can imagine, however there is only one now.

### How do I get set up? ###

Install it using MSI package and then you can proceed straight to the next section or build from the source codes by cloning the repository's source code first. Once you are done with cloning, launch **Fiddler.Plugins.sln** solution file, ensure Fiddler is not running before you start building and then build the entire solution (F6 is a keyboard shortcut in Visual Studio). The repository has all the necessary dependencies in order to successfully build the solution, so you should not encounter any issues with missing references during the build.

Also, **Fiddler.ClientCertificates.csproj** file is configured to place the build output into the Fiddler's Scripts folder (where all the plugins live) and once the build is successfully completed you will have the plugin in the right place.

## Configure the plugin ##
To properly configure the plugin you need to launch Fiddler as Administrator - this is needed for the Fiddler's process to be able to export public part of the certificate.

### Add Host Certificate Auth Mapping Command ###

Syntax: mod mapping <hostname> <thumbprint>

Example: mod mapping mywebservice.com 090909909909990

### Disable Plugin Command ###

Syntax: mod disable

Example: mod disable

### Enable Plugin Command ###

Syntax: mod enable

Example: mod enable

### Get Help with Plugin ###

Syntax: mod help

Example: mod help

## Troubleshooting Hints ##

To troubleshoot the plugin persisted configuration, browse or modify it manually use the following Quick Exec command:

**about:config**

To troubleshoot runtime-related issues with the plugin using Log tab, use the following Quick Exec command:

**prefs set fiddler.debug.extensions.verbose true**

To troubleshoot startup-related issues with the plugin and output all the exceptions encountered in the runtime, use the following Quick Exec command:

**prefs set fiddler.debug.extensions.showerrors true**

Hope you will find it useful! Enjoy!