﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fiddler.ClientCertificates;
using Fiddler.ClientCertificates.Tests.Common;
using Fiddler.ClientCertificates.Tests.Extensions;
using Newtonsoft.Json;
using Moq;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace Fiddler.ClientCertificates.Tests
{
    [TestClass]
    public class HandleExecActionTestSuite
    {
        private TestableCertificatesPlugin plugin;
        private string thumbprint;
        private string filePath;

        [TestInitialize]
        public void Initialize()
        {
            CONFIG.QuietMode = true;

            this.plugin = new TestableCertificatesPlugin();
            this.thumbprint = "8D054D2434902E30473ADE367FFA25279126B3EE";
            this.filePath = this.BuildExportedCertificateFilePath(this.thumbprint);
        }

        [TestMethod]
        public void ShouldHandleHelpCommand()
        {
            // Arrange
            bool expected = true;
            string sCommand = this.BuildExecActionHelpCommand();

            // Act
            bool actual = this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldHandleHelpCommandAndLaunchCorrectHelpLink()
        {
            // Arrange
            string expected = MyCertificatesPlugin.HelpLink;
            string actual = null;
            string sCommand = this.BuildExecActionHelpCommand();
            this.plugin.SetupLaunchHelpLink(s => actual = s);

            // Act
            this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldHandleSetMappingCommand()
        {
            // Arrange
            bool expected = true;
            string sCommand = this.BuildExecActionMappingCommand("google.com " + this.thumbprint);
            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference();

            // Act
            bool actual = this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldIgnoreUnknownExecActionCommand()
        {
            // Arrange
            bool expected = false;
            string sCommand = "asd 123 123";

            // Act
            bool actual = this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldInjectNewMappingUsingThumbprint()
        {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ShouldInjectNewMappingGivenValidCertificateThumbprint()
        {
            // Arrange
            string sCommand = this.BuildExecActionMappingCommand("google.com " + this.thumbprint);
            Mapping expected = new Mapping() { Hostname = "google.com", CertificatePath = this.filePath };
            Mapping actual = null;

            this.plugin.SetupSaveDisabledPreference();

            // Act
            this.plugin.SetupSaveMappingsPreference(saveSpy: (m) => actual = m.First());
            this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Hostname, actual.Hostname);
            Assert.AreEqual(expected.CertificatePath.ToLowerInvariant(), actual.CertificatePath.ToLowerInvariant());
        }

        [TestMethod]
        public void ShouldUseThumbprintAsFileNameToSaveCertificate()
        {
            // Arrange
            string sCommand = this.BuildExecActionMappingCommand("google.com " + this.thumbprint);
            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference();

            // Act
            this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.IsTrue(File.Exists(this.filePath));
        }

        [TestMethod]
        public void ShouldSavePublicPartOfCertificateToFile()
        {
            // Arrange
            string sCommand = this.BuildExecActionMappingCommand("google.com " + this.thumbprint);
            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference();

            // Act
            this.plugin.OnExecAction(sCommand);
            X509Certificate2 certificate = new X509Certificate2(this.filePath);

            // Assert
            this.plugin.Verify();
            Assert.IsFalse(certificate.HasPrivateKey);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ShouldThrowInvalidOperationExceptionIfThumbprintIsNotValid()
        {
            // Arrange
            string sCommand = this.BuildExecActionMappingCommand("google.com xxxxx");

            // Act
            this.plugin.OnExecAction(sCommand);

            // Assert
        }

        [TestMethod]
        public void ShouldHandleEnablePluginCommand()
        {
            // Arrange
            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference();

            bool expectedInitialState = false;
            bool actualInitialState = this.plugin.Disabled;

            bool expectedArrangedState = true;
            this.plugin.OnExecAction(this.BuildExecActionDisableCommand());
            bool actualArrangedState = this.plugin.Disabled;

            string sCommand = this.BuildExecActionEnableCommand();

            // Act
            bool expectedFinalState = false;
            this.plugin.OnExecAction(sCommand);
            bool actualFinalState = this.plugin.Disabled;

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expectedInitialState, actualInitialState, "Initial state has issues");
            Assert.AreEqual(expectedArrangedState, actualArrangedState, "Arranged state has issues");
            Assert.AreEqual(expectedFinalState, actualFinalState, "Final state has issues");
        }

        [TestMethod]
        public void ShouldHandleEnablePluginCommandAndSavePreference()
        {
            // Arrange
            bool expected = false;
            bool actual = !expected; // it's a safety net - just in case setup won't be executed

            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference(saveSpy: (value) => actual = value);

            string sCommand = this.BuildExecActionEnableCommand();

            // Act
            this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual, "The preferences is expected to be saved");
        }

        [TestMethod]
        public void ShouldHandleDisablePluginCommand()
        {
            // Arrange
            bool expectedInitialState = false;
            bool actualInitialState = this.plugin.Disabled;

            string sCommand = this.BuildExecActionDisableCommand();

            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference();

            // Act
            bool expectedFinalState = true;
            this.plugin.OnExecAction(sCommand);
            bool actualFinalState = this.plugin.Disabled;

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expectedInitialState, actualInitialState, "Initial state has issues");
            Assert.AreEqual(expectedFinalState, actualFinalState, "Final state has issues");
        }

        [TestMethod]
        public void ShouldHandleDisablePluginCommandAndSavePreference()
        {
            // Arrange
            bool expected = true;
            bool actual = !expected; // it's a safety net - just in case setup won't be executed

            this.plugin.SetupSaveMappingsPreference();
            this.plugin.SetupSaveDisabledPreference(saveSpy: (value) => actual = value);

            string sCommand = this.BuildExecActionDisableCommand();

            // Act
            this.plugin.OnExecAction(sCommand);

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual, "The preferences is expected to be saved");
        }

        [TestMethod]
        public void ShouldUseDefaultLocationToSavePublicPartOfCertificate()
        {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ShouldHandleReadMappingCommand()
        {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ShouldHandleRemoveMappingCommand()
        {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ShouldHandleResetPluginCommand()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// Should handle location command to update the location where certificates are saved to and read from.
        /// </summary>
        [TestMethod]
        public void ShouldHandleSetCertificatesLocationCommand()
        {
            Assert.Inconclusive();
        }

        private string BuildExportedCertificateFilePath(string thumbprint)
        {
            return @"c:\" + thumbprint + ".cer";
        }

        private string BuildExecActionDisableCommand()
        {
            return this.BuildExecActionCommand(MyCertificatesPlugin.ExecActionDisableCommand);
        }

        private string BuildExecActionMappingCommand(string args)
        {
            // <mod> mapping google.com 123
            return this.BuildExecActionCommand(MyCertificatesPlugin.ExecActionMappingCommand, args);
        }

        private string BuildExecActionEnableCommand()
        {
            // <mod> mapping google.com 123
            return this.BuildExecActionCommand(MyCertificatesPlugin.ExecActionEnableCommand);
        }

        private string BuildExecActionHelpCommand()
        {
            // <mod> help
            return this.BuildExecActionCommand(MyCertificatesPlugin.ExecActionHelpCommand);
        }

        private string BuildExecActionCommand(string command, string args = null)
        {
            // <mod> command args
            if (args != null)
            {
                return MyCertificatesPlugin.ExecActionNamespace + " " + command + " " + args;
            }

            return MyCertificatesPlugin.ExecActionNamespace + " " + command;
        }
    }
}
