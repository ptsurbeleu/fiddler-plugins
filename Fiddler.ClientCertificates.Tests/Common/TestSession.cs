﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiddler.ClientCertificates.Tests.Common
{
    public class TestSession : Session
    {
        public TestSession()
            : base(arrRequest: RequestBytes, arrResponse: ResponseBytes)
        {
        }

        #region Constants
        private static readonly byte[] RequestBytes = Encoding.ASCII.GetBytes(@"GET http://www.telerik.com/UpdateCheck.aspx?isBeta=False HTTP/1.1
User-Agent: Fiddler/4.4.8.0 (.NET 4.0.30319.34014; WinNT 6.3.9600.0; en-US; 4xAMD64)
Pragma: no-cache
Accept-Language: en-US
Referer: http://fiddler2.com/client/4.4.8.0
Host: www.telerik.com
Accept-Encoding: gzip, deflate
Connection: Close

");
        private static readonly byte[] ResponseBytes = Encoding.UTF8.GetBytes(@"HTTP/1.1 200 OK
Cache-Control: private
Content-Type: text/plain; charset=utf-8
Vary: Accept-Encoding
Server: Microsoft-IIS/7.5
Date: Wed, 14 May 2014 06:18:20 GMT
Content-Length: 433
Connection: close

");
        #endregion
    }
}
