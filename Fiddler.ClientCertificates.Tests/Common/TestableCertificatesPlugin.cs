﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Newtonsoft.Json;
using Fiddler.ClientCertificates.Tests.Extensions;

namespace Fiddler.ClientCertificates.Tests.Common
{
    public class TestableCertificatesPlugin : MyCertificatesPlugin
    {
        private Action<string> launchSpy;
        public Mock<IFiddlerPreferences> MockPreferences { get; private set; }

        public TestableCertificatesPlugin()
            : base()
        {
        }

        protected override void ResolveDependencies()
        {
            this.MockPreferences = new Mock<IFiddlerPreferences>(MockBehavior.Strict);
            this.Preferences = this.MockPreferences.Object;
        }

        public void SetupLaunchHelpLink(Action<string> launchSpy)
        {
            this.launchSpy = launchSpy;
        }

        protected override void LaunchHyperlink(string value)
        {
            if (this.launchSpy != null)
            {
                this.launchSpy(value);
            }
        }

        public void SetupReadMappingsPreference(params Mapping[] mappings)
        {
            var jsonString = JsonConvert.SerializeObject(mappings);
            this.MockPreferences.SetupGetStringPref(MappingsKey, jsonString);
        }

        public void Verify()
        {
            this.MockPreferences.Verify();
        }

        public void SetupSaveMappingsPreference(Action<IEnumerable<Mapping>> saveSpy = null)
        {
            Action<string> setterSpy = null;

            if (saveSpy != null)
            {
                setterSpy = (jsonString) =>
                {
                    var mappings = JsonConvert.DeserializeObject<List<Mapping>>(jsonString);
                    saveSpy(mappings.AsEnumerable());
                };
            }

            this.MockPreferences.SetupSetStringPref(MappingsKey, setterSpy: setterSpy);
        }

        public void SetupSaveDisabledPreference(Action<bool> saveSpy = null)
        {
            this.MockPreferences.SetupSetBoolPref(PluginDisabledKey, setterSpy: saveSpy);
        }

        public void SetupReadDisabledPreference(bool expected = false)
        {
            this.MockPreferences.SetupGetBoolPref(PluginDisabledKey, expected);
        }
    }
}
