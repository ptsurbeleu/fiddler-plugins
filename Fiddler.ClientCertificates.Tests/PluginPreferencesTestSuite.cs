﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fiddler.ClientCertificates;
using Fiddler.ClientCertificates.Tests.Common;
using Fiddler.ClientCertificates.Tests.Extensions;
using Newtonsoft.Json;
using Moq;

namespace Fiddler.ClientCertificates.Tests
{
    [TestClass]
    public class PluginPreferencesTestSuite
    {
        private TestableCertificatesPlugin plugin;

        [TestInitialize]
        public void Initialize()
        {
            CONFIG.QuietMode = true;

            this.plugin = new TestableCertificatesPlugin();
        }

        [TestMethod]
        public void ShouldReadMappingsSettingFromPreferences()
        {
            // Arrange
            var expected = new Mapping { Hostname = "ddd.com", CertificatePath = "d" };
            this.plugin.SetupReadMappingsPreference(expected);
            this.plugin.SetupReadDisabledPreference();

            // Act
            this.plugin.OnLoad();
            var actual = this.plugin.MatchMappingByHostname(expected.Hostname);

            // Assert
            this.plugin.Verify();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Hostname, actual.Hostname);
            Assert.AreEqual(expected.CertificatePath, actual.CertificatePath);
        }

        [TestMethod]
        public void ShouldReadDisabledSettingFromPreferences()
        {
            // Arrange
            var expected = true;
            this.plugin.SetupReadMappingsPreference();
            this.plugin.SetupReadDisabledPreference(expected);

            // Act
            this.plugin.OnLoad();
            var actual = this.plugin.Disabled;

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual);
        }
    }
}
