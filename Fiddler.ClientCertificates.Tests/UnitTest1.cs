﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fiddler.ClientCertificates;
using Fiddler.ClientCertificates.Tests.Common;
using Fiddler.ClientCertificates.Tests.Extensions;
using Newtonsoft.Json;

namespace Fiddler.ClientCertificates.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private TestableCertificatesPlugin plugin;
        private Session oSession;

        [TestInitialize]
        public void Initialize()
        {
            CONFIG.QuietMode = true;

            this.plugin = new TestableCertificatesPlugin();
            this.plugin.SetupReadDisabledPreference();

            this.oSession = this.GetInstanceOfSession();
        }

        [TestMethod]
        public void ShouldSetSessionFlagIfHostnameMatches()
        {
            // Arrange
            var firstHostname = "xxx.com";
            var secondHostname = "aaa.com";

            this.plugin.SetupReadMappingsPreference(
                new Mapping { Hostname = firstHostname, CertificatePath = "d" },
                new Mapping { Hostname = secondHostname, CertificatePath = "w" });
            this.plugin.OnLoad();

            this.oSession.hostname = firstHostname;

            // Act
            this.plugin.AutoTamperRequestBefore(oSession);

            // Assert
            this.plugin.Verify();
            Assert.IsNotNull(oSession.ClientCertificate().Get());
        }

        [TestMethod]
        public void ShouldAttachClientCertificateIfHostnameMatch()
        {
            // Arrange
            var expected = "z";

            var firstHostname = "rrr.com";
            var secondHostname = "qqq.com";

            this.plugin.SetupReadMappingsPreference(
                new Mapping { Hostname = firstHostname, CertificatePath = expected },
                new Mapping { Hostname = secondHostname, CertificatePath = expected });
            this.plugin.OnLoad();

            this.oSession.hostname = firstHostname;

            // Act
            this.plugin.AutoTamperRequestBefore(oSession);
            var actual = oSession.ClientCertificate().Get();

            // Assert
            this.plugin.Verify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldNotAttachClientCertificateIfNoHostnameMatch()
        {
            // Arrange
            this.plugin.SetupReadMappingsPreference(new Mapping { Hostname = "xxx.com", CertificatePath = "ccc" });
            this.plugin.OnLoad();

            this.oSession.hostname = "dddd.com";

            // Act
            this.plugin.AutoTamperRequestBefore(oSession);

            // Assert
            this.plugin.Verify();
            Assert.IsNull(oSession.ClientCertificate().Get());
        }

        [TestMethod]
        public void ShouldSetSkipSessionDecryptionFlagIfNoHostnameMatch()
        {
            // Arrange
            string expected = bool.TrueString;
            this.plugin.SetupReadMappingsPreference(new Mapping { Hostname = "xxx.com", CertificatePath = "ccc" });
            this.plugin.OnLoad();

            this.oSession.hostname = "dddd.com";

            // Act
            this.plugin.AutoTamperRequestBefore(oSession);
            string actual = oSession.NoDecrypt().Get();

            // Assert
            this.plugin.Verify();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldNotAttachClientCertificateIfPreferencesEmpty()
        {
            // Arrange
            this.plugin.SetupReadMappingsPreference();
            this.plugin.OnLoad();

            // Act
            this.plugin.AutoTamperRequestBefore(oSession);

            // Assert
            this.plugin.Verify();
            Assert.IsNull(oSession.ClientCertificate().Get());
        }

        private Session GetInstanceOfSession()
        {
            return new TestSession();
        }
    }
}
