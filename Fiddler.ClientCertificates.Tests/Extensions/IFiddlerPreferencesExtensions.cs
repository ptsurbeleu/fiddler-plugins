﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace Fiddler.ClientCertificates.Tests.Extensions
{
    public static class IFiddlerPreferencesExtensions
    {
        public static void SetupGetStringPref(this Mock<IFiddlerPreferences> mock, string name, string value)
        {
            mock
                .Setup(p => p.GetStringPref(It.Is<string>(x => string.Equals(x, name)), It.IsAny<string>()))
                .Returns(value)
                .Verifiable();
        }

        public static void SetupGetBoolPref(this Mock<IFiddlerPreferences> mock, string name, bool value)
        {
            mock
                .Setup(p => p.GetBoolPref(It.Is<string>(x => string.Equals(x, name)), It.IsAny<bool>()))
                .Returns(value)
                .Verifiable();
        }

        public static void SetupSetStringPref(this Mock<IFiddlerPreferences> mock, string name, string value = null, Action<string> setterSpy = null)
        {
            var setup = mock.Setup(p => p.SetStringPref(It.Is<string>(x => string.Equals(x, name)), It.IsAny<string>()));
            if (setterSpy != null)
            {
                setup.Callback((string sPrefname, string sValue) => setterSpy(sValue));
            }

            setup.Verifiable();
        }

        public static void SetupSetBoolPref(this Mock<IFiddlerPreferences> mock, string name, bool value = false, Action<bool> setterSpy = null)
        {
            var setup = mock.Setup(p => p.SetBoolPref(It.Is<string>(x => string.Equals(x, name)), It.IsAny<bool>()));
            if (setterSpy != null)
            {
                setup.Callback((string sPrefname, bool sValue) => setterSpy(sValue));
            }

            setup.Verifiable();
        }
    }
}
